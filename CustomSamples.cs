﻿using SampleQueries;
using SampleSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace QuerySamples
{
    [Title("LINQ Query Samples")]
    [Prefix("Linq")]
    public class CustomSamples : SampleHarness
    {
        private LinqSamples samples = new LinqSamples();

        [Category("Restriction operators")]
        [Title("Where 1")]
        [Description("Gets list of clients with total order sum more than given number. Selects CustomerId, OrderId and Order sum.")] 
        public void LinqQuery1()
        {
            decimal orderSum = 10;

            var customers = this.samples.GetCustomerList();

            var res = customers.Where(x => x.Orders.Sum(y => y.Total) > orderSum)
                               .SelectMany(x => x.Orders, (x, y) => (x.CustomerID, y.OrderID, y.Total));

            foreach (var item in res)
            {
                Console.WriteLine($"ID : {item.CustomerID} | OrderId : {item.OrderID} | Order sum : {item.Total}");
            }
        }

        [Category("Restriction operators")]
        [Title("Where 2")]
        [Description("For each customer gets list of suppliers in the same city and country. Removes empty entries.")]
        public void LinqQuery2()
        {
            var customers = this.samples.GetCustomerList();
            var suppliers = this.samples.GetSupplierList();

            var result = customers.GroupJoin(suppliers, x => (x.Country, x.City),
                y => (y.Country, y.City), (customer, suppliersList) => (customer, suppliersList))
                .Where(x => x.suppliersList.Count() != 0);

            foreach (var item in result)
            {
                Console.WriteLine($"Customer Id : {item.customer.CustomerID}");
                Console.WriteLine("Suppliers list:");
                
                foreach (var supplier in item.suppliersList)
                {
                    Console.WriteLine(supplier.SupplierName);
                }

                Console.WriteLine();
            }
        }


        [Category("Restriction operators")]
        [Title("Where 2.1")]
        [Description("For each customer gets list of suppliers in the same city and country without using grouping")]
        public void LinqQuery21()
        {
            var customers = this.samples.GetCustomerList();
            var suppliers = this.samples.GetSupplierList();

            var result = customers.Select(x => (x.CustomerID,
                suppliers.Where(y => string.Equals(x.Country, y.Country) && string.Equals(x.City, y.City))));

            foreach (var item in result)
            {
                Console.WriteLine($"CustomerId: {item.CustomerID}");
                Console.WriteLine("Suppliers list:");

                foreach (var supplier in item.Item2)
                {
                    Console.WriteLine(supplier.SupplierName);
                }

                Console.WriteLine();
            }
        }

        [Category("Restriction operators")]
        [Title("Where 3")]
        [Description("Gets list of customers whose all order sums are bigger than given number.")]
        public void LinqQuery3()
        {
            var customers = this.samples.GetCustomerList();
            decimal minSum = 150;

            var result = customers.Where(x => x.Orders.All(y => y.Total > minSum));

            foreach (var customer in result)
            {
                Console.WriteLine($"CustomerId: {customer.CustomerID}");
            }
        }

        [Category("Ordering operators")]
        [Title("OrderBy 1")]
        [Description("Orders customers by first order's year.")]
        public void LinqQuery4_1()
        {
            var customers = this.samples.GetCustomerList();

            var result = customers.Where(x => x.Orders.Length != 0)
                .OrderBy(x => x.Orders.OrderBy(y => y.OrderDate).First().OrderDate.Year);

            foreach (var customer in result)
            {
                Console.WriteLine($"CustomerId: {customer.CustomerID}");
                Console.WriteLine("Orders:");

                foreach (var order in customer.Orders)
                {
                    Console.WriteLine($"OrderId: {order.OrderID} | OrderDate: {order.OrderDate}");
                }

                Console.WriteLine();
            }
        }


        [Category("Ordering operators")]
        [Title("OrderBy 2")]
        [Description("Orders customers by first order's month.")]
        public void LinqQuery4_2()
        {
            var customers = this.samples.GetCustomerList();

            var result = customers.Where(x => x.Orders.Length != 0)
                .OrderBy(x => x.Orders.OrderBy(y => y.OrderDate).First().OrderDate.Month);

            foreach (var customer in result)
            {
                Console.WriteLine($"CustomerId: {customer.CustomerID}");
                Console.WriteLine("Orders:");

                foreach (var order in customer.Orders)
                {
                    Console.WriteLine($"OrderId: {order.OrderID} | OrderDate: {order.OrderDate}");
                }

                Console.WriteLine();
            }
        }

        [Category("Ordering operators")]
        [Title("OrderBy 3")]
        [Description("Orders customers by total orders sum.")]
        public void LinqQuery4_3()
        {
            var customers = this.samples.GetCustomerList();

            var result = customers.Select(x => (x.CustomerID, x.Orders.Sum(y => y.Total)))
                .OrderByDescending(x => x.Item2);

            foreach (var customer in result)
            {
                Console.WriteLine($"CustomerId: {customer.CustomerID} | Total orders sum: {customer.Item2}");
            }
        }

        [Category("Ordering operators")]
        [Title("OrderBy 4")]
        [Description("Orders customers by total orders sum.")]
        public void LinqQuery4_4()
        {
            var customers = this.samples.GetCustomerList();

            foreach (var customer in customers.OrderBy(x => x.CustomerID))
            {
                Console.WriteLine($"CustomerId: {customer.CustomerID}");
            }
        }

        [Category("Restriction Operators")]
        [Title("Where 4")]
        [Description("Orders customers by total orders sum.")]
        public void LinqQuery5()
        {
            var customers = this.samples.GetCustomerList();

            var result = customers.Where(x => string.IsNullOrEmpty(x.Region)
                                              || !string.IsNullOrEmpty(x.PostalCode) && !x.PostalCode.All(char.IsDigit)
                                              || !string.IsNullOrEmpty(x.Phone) && x.Phone[0] != '(');
            
            foreach (var customer in result)
            {
                Console.WriteLine($"CustomerId: {customer.CustomerID}");
                Console.WriteLine("Info:");
                Console.WriteLine($"Phone: {customer.Phone}");
                Console.WriteLine($"Region: {customer.Region}");
                Console.WriteLine($"Postal code: {customer.PostalCode}");

                Console.WriteLine();
            }
        }

        [Category("Grouping Operators")]
        [Title("GroupBy 1")]
        [Description("Groups products by categories, then by units in stock, then by unit price.")]
        public void LinqQuery6()
        {
            var products = this.samples.GetProductList();

            var query = from product in products
                group product by product.Category into categories
                    select new
                    {
                        Category = categories.Key,
                        CountGroups = from product in categories
                            group product by product.UnitsInStock into unitsInStock
                                select new
                                {
                                    UnitsInStock = unitsInStock.Key,
                                    PriceGroups = from product in unitsInStock
                                        group product by product.UnitPrice into priceGroups
                                            select new
                                            {
                                                Price = priceGroups.Key,
                                                Products = priceGroups
                                            }
                                }
                    };
            
            ObjectDumper.Write(query, 4);
        }


        [Category("Grouping Operators")]
        [Title("GroupBy 2")]
        [Description("Groups products by their price.")]
        public void LinqQuery7()
        {
            var products = this.samples.GetProductList();

            decimal lowPrice = 15;
            decimal highPrice = 50;

            var query = from product in products
                group product by GetCategory(product.UnitPrice) into priceGroups
                select new
                {
                    Group = priceGroups.Key,
                    Products = priceGroups
                };

            ObjectDumper.Write(query, 2);

            string GetCategory(decimal price)
            {
                if (price < lowPrice)
                {
                    return "Cheap price";
                }

                return price > highPrice ? "Expensive price" : "Middle price";
            }
        }

        [Category("Math Operators")]
        [Title("Average 1")]
        [Description("Counts average order sum for each city.")]
        public void LinqQuery8_1()
        {
            var customers = this.samples.GetCustomerList();

            var query = from customer in customers
                group customer by customer.City
                into customersByCities
                select new
                {
                    City = customersByCities.Key,
                    AverageOrderPrice = customersByCities.Select(x=> x.Orders.Length ==0 ? 0 : x.Orders.Average(y => y.Total)).Average(x => x)
                };

            foreach (var item in query)
            {
                Console.WriteLine($"City: {item.City} | Average order price: {item.AverageOrderPrice}");
            }
        }


        [Category("Math Operators")]
        [Title("Average 2")]
        [Description("Counts average count sum for each city.")]
        public void LinqQuery8_2()
        {
            var customers = this.samples.GetCustomerList();

            var query = from customer in customers
                group customer by customer.City
                into cityGroups
                select new
                {
                    City = cityGroups.Key,
                    AverageOrderCount = cityGroups.Select(x => x.Orders.Length).Average(x => x)
                };
            
            foreach (var item in query)
            {
                Console.WriteLine($"City: {item.City} | Average order count: {item.AverageOrderCount}");
            }
        }

        [Category("Various Operators")]
        [Title("Where")]
        [Description("Gets suppliers where one or more fields are empty.")]
        public void LinqQuery9_1()
        {
            var suppliers = this.samples.GetSupplierList();

            var query = suppliers.Where(x => string.IsNullOrEmpty(x.Address)
                                             || string.IsNullOrEmpty(x.City)
                                             || string.IsNullOrEmpty(x.Country)
                                             || string.IsNullOrEmpty(x.SupplierName));
            foreach (var supplier in query)
            {
                Console.WriteLine($"Supplier name: {supplier.SupplierName}");
                Console.WriteLine($"Supplier city: {supplier.City}");
                Console.WriteLine($"Supplier country: {supplier.Country}");
                Console.WriteLine($"Supplier adress: {supplier.Address}");
                Console.WriteLine();
            }
        }

        [Category("Various Operators")]
        [Title("Same letter")]
        [Description("Gets suppliers where city and country starts with the same letter.")]
        public void LinqQuery9_2()
        {
            var suppliers = this.samples.GetSupplierList();

            var query = suppliers.Where(x => x.City[0] == x.Country[0]);

            foreach (var supplier in query)
            {
                Console.WriteLine($"{supplier.City} - {supplier.Country}");
            }
        }

        [Category("Various Operators")]
        [Title("Group + Select")]
        [Description("Gets max order count from every city.")]
        public void LinqQuery9_3()
        {
            var customers = this.samples.GetCustomerList();

            var query = from customer in customers
                group customer by customer.City
                into cityGroups
                select new
                {
                    City = cityGroups.Key,
                    MaxOrders = cityGroups.Select(x => x.Orders.Count()).Max()
                };

            foreach (var item in query)
            {
                Console.WriteLine($"City: {item.City} | Max orders: {item.MaxOrders}");
            }
        }

        [Category("Various Operators")]
        [Title("Where 2")]
        [Description("Gets orders where order date is bigger than specified date.")]
        public void LinqQuery9_4()
        {
            DateTime date = new DateTime(1997, 1, 1);
            var customers = this.samples.GetCustomerList();

            var query = customers.Select(x => (x.CustomerID, x.Orders.Where(y => y.OrderDate >= date)));

            foreach (var item in query)
            {
                Console.WriteLine($"Customer ID: {item.CustomerID}");
                foreach (var order in item.Item2)
                {
                    Console.WriteLine($"OrderId : {order.OrderID} | Order date: {order.OrderDate}");
                }

                Console.WriteLine();
            }
        }

        [Category("Various Operators")]
        [Title("Take")]
        [Description("Gets 3 max by price orders for each customer.")]
        public void LinqQuery9_5()
        {
            var customers = this.samples.GetCustomerList();

            var query = customers.Select(x => (x.CustomerID, x.Orders.OrderByDescending(y => y.Total).Take(3)));

            foreach (var item in query)
            {
                Console.WriteLine($"Customer ID: {item.CustomerID}");

                foreach (var order in item.Item2)
                {
                    Console.WriteLine($"Order ID: {order.OrderID} | Total: {order.Total}");
                }

                Console.WriteLine();
            }
        }
    }
}
